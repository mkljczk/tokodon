# Tokodon

A modern client for [Mastodon](https://joinmastodon.org/) and other
decentralized servers that implement its API (such as Pixelfed).

<a href='https://flathub.org/apps/details/org.kde.tokodon'><img width='190px' alt='Download on Flathub' src='https://flathub.org/assets/badges/flathub-badge-i-en.png'/></a>

![coverage](https://invent.kde.org/network/tokodon/badges/master/coverage.svg?job=suse_tumbleweed_qt515)

![Timeline](https://cdn.kde.org/screenshots/tokodon/tokodon-desktop.png)

## Features

* Real-time notifications, including background push notifications (using [KUnifiedPush](https://invent.kde.org/libraries/kunifiedpush).)
* Direct messages.
* Editing & deleting toots.
* Multiple accounts and cross-account actions.
* Searching for users, hashtags and posts.
* Moderation tools like viewing a server's accounts, email blocks and more. 

## Get It

Details on where to find stable releases of Tokodon can be found on its
[homepage](https://apps.kde.org/tokodon). An Android version can be found
in the [KDE Nightly F-Droid repository](https://community.kde.org/Android/F-Droid).

## Support

If you have an issue with Tokodon, please [open a support thread on KDE Discuss](https://discuss.kde.org/tag/tokodon). Alternatively, you ask in the [Tokodon Matrix room](https://go.kde.org/matrix/#/#tokodon:kde.org). See [Matrix](https://community.kde.org/Matrix) for more details.

## Building

The easiest way to make changes and test Tokodon during development is to [build it with kdesrc-build](https://community.kde.org/Get_Involved/development/Build_software_with_kdesrc-build).

## Contributing

Please refer to the [contributing document](/CONTRIBUTING.md) for everything you need to know to get started contributing to Tokodon.

## License

![GPLv3](https://www.gnu.org/graphics/gplv3-127x51.png)

This project is licensed under the GNU General Public License 3. The logo was done by Bugsbane and licensed under
CC-BY-4.0.
